## TP6 : Un LAN maîtrisé meo ?

**🌞 Dans le rendu, je veux**

 **cat named.conf**  

``` shell
 [matheo@dns ~]$ sudo cat /etc/named.conf  

 options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };
        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
 };

 logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
 };

 zone "tp6.b1" IN {
     type master;
     file "tp6.b1.db";
     allow-update { none; };
     allow-query {any; };
 };
 zone "1.4.10.in-addr.arpa" IN {
     type master;
     file "tp6.b1.rev";
     allow-update { none; };
     allow-query { any; };
 };
```

 **cat tp6.b1.db**

``` shell
 [root@dns ~]# cat /var/named/tp6.b1.db
 $TTL 86400
 @ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
 )

 @ IN NS dns.tp6.b1.

 dns       IN A 10.6.1.101
 john      IN A 10.6.1.11
 ```
 
 **cat tp6.b1.rev**

``` shell
 [root@dns ~]# cat /var/named/tp6.b1.rev
 $TTL 86400
 @ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
 )

 @ IN NS dns.tp6.b1.

 101 IN PTR dns.tp6.b1.
 11 IN PTR john.tp6.b1.
```

 **un systemctl status named qui prouve que le service tourne bien**

``` shell
 [matheo@dns ~]$ systemctl status named
 ● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; preset: disabled)
     Active: active (running) since Thu 2023-11-23 15:30:53 CET; 1min 38s ago
   Main PID: 1288 (named)
      Tasks: 4 (limit: 4606)
     Memory: 21.6M
        CPU: 35ms
     CGroup: /system.slice/named.service
             └─1288 /usr/sbin/named -u named -c /etc/named.conf

 Nov 23 15:30:53 dns.tp6.b1 named[1288]: configuring command channel from '/etc/rndc.key'
 Nov 23 15:30:53 dns.tp6.b1 named[1288]: command channel listening on 127.0.0.1#953
 Nov 23 15:30:53 dns.tp6.b1 named[1288]: configuring command channel from '/etc/rndc.key'
 Nov 23 15:30:53 dns.tp6.b1 named[1288]: command channel listening on ::1#953
 Nov 23 15:30:53 dns.tp6.b1 named[1288]: managed-keys-zone: loaded serial 0
 Nov 23 15:30:53 dns.tp6.b1 named[1288]: zone 1.4.10.in-addr.arpa/IN: loaded serial 2019061800
 Nov 23 15:30:53 dns.tp6.b1 named[1288]: zone tp6.b1/IN: loaded serial 2019061800
 Nov 23 15:30:53 dns.tp6.b1 named[1288]: all zones loaded
 Nov 23 15:30:53 dns.tp6.b1 systemd[1]: Started Berkeley Internet Name Domain (DNS).
 Nov 23 15:30:53 dns.tp6.b1 named[1288]: running
```
``` shell
[matheo@dns ~]$ sudo ss -ltupn
[sudo] password for matheo:
Netid   State    Recv-Q   Send-Q       Local Address:Port       Peer Address:Port   Process
udp     UNCONN   0        0                127.0.0.1:323             0.0.0.0:*       users:(("chronyd",pid=662,fd=5))
udp     UNCONN   0        0               10.6.1.101:53              0.0.0.0:*       users:(("named",pid=1288,fd=19))
udp     UNCONN   0        0                127.0.0.1:53              0.0.0.0:*       users:(("named",pid=1288,fd=16))
udp     UNCONN   0        0                    [::1]:323                [::]:*       users:(("chronyd",pid=662,fd=6))
udp     UNCONN   0        0                    [::1]:53                 [::]:*       users:(("named",pid=1288,fd=22))
tcp     LISTEN   0        10              10.6.1.101:53              0.0.0.0:*       users:(("named",pid=1288,fd=21))
tcp     LISTEN   0        4096             127.0.0.1:953             0.0.0.0:*       users:(("named",pid=1288,fd=24))
tcp     LISTEN   0        10               127.0.0.1:53              0.0.0.0:*       users:(("named",pid=1288,fd=17))
tcp     LISTEN   0        128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=677,fd=3))
tcp     LISTEN   0        10                   [::1]:53                 [::]:*       users:(("named",pid=1288,fd=23))
tcp     LISTEN   0        128                   [::]:22                 [::]:*       users:(("sshd",pid=677,fd=4))
tcp     LISTEN   0        4096                 [::1]:953                [::]:*       users:(("named",pid=1288,fd=25))
```

**🌞 Ouvrez le bon port dans le firewall**
```
 [matheo@dns ~]$ sudo firewall-cmd --add-port=53/udp --permanent
 success  
```
## 3. Test

**🌞 Sur la machine john.tp6.b1**
```
 [matheo@john ~]$ sudo cat /etc/resolv.conf
 nameserver 10.6.1.101
```
**résoudre des noms**
``` shell
[matheo@john ~]$ ping www.ynov.com
PING www.ynov.com (172.67.74.226) 56(84) bytes of data.
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=1 ttl=55 time=25.3 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=2 ttl=55 time=41.3 ms
--- www.ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 25.275/33.288/41.302/8.013 ms
```

**🌞 Sur votre PC**


**🌞 Installer un serveur DHCP**

```
[matheo@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf

# create new
# specify domain name
option domain-name     "TP6_DHCP";
# specify DNS server's hostname or IP address
option domain-name-servers     10.6.1.101;
# default lease time
default-lease-time 21600;
# max lease time
max-lease-time 30000;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.6.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.6.1.13 10.6.1.37;
    # specify broadcast address
    option broadcast-address 10.6.1.255;
    # specify gateway
    option routers 10.6.1.254;
}
```

**🌞 Test avec john.tp6.b1**

```
 enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:fb:03:28 brd ff:ff:ff:ff:ff:ff
    inet 10.6.1.13/24 brd 10.6.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 21365sec preferred_lft 21365sec
    inet6 fe80::a00:27ff:fefb:328/64 scope link
       valid_lft forever preferred_lft forever
```

```
[matheo@jhon ~]$ ip r s
default via 10.6.1.254 dev enp0s3
default via 10.6.1.254 dev enp0s3 proto dhcp src 10.6.1.13 metric 100
10.6.1.0/24 dev enp0s3 proto kernel scope link src 10.6.1.13 metric 100
```
```
[matheo@jhon ~]$ ping www.google.com
PING www.google.com (172.217.20.196) 56(84) bytes of data.
64 bytes from par10s50-in-f4.1e100.net (172.217.20.196): icmp_seq=1 ttl=115 time=13.1 ms
64 bytes from waw02s08-in-f196.1e100.net (172.217.20.196): icmp_seq=2 ttl=115 time=23.5 m

```