## I. ARP

## 1. Echange ARP

**1. Echange ARP**
``` shell

    [matheo@localhost ~]$ ping 10.3.1.12
    PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
    64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.887 ms
    64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.751 ms
    64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.737 ms
    64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=0.806 ms
    ^C
    --- 10.3.1.12 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3058ms  
    rtt min/avg/max/mdev = 0.737/0.795/0.887/0.058 ms  
 ```    
john :  

    [matheo@localhost ~]$ ip n s  
    10.3.1.12 dev enp0s3 lladdr 08:00:27:c0:77:41 STALE  
    \_MAC de Marcel_/  
    10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:30 REACHABLE  

marcel : 

    [matheo@localhost ~]$ ip n s  
    10.3.1.11 dev enp0s3 lladdr 08:00:27:85:1a:f6 STALE  
    \__MAC de john__/  
    10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:30 DELAY  
  
    [matheo@localhost ~]$ ip a  
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000  
    link/ether 08:00:27:c0:77:41 brd ff:ff:ff:ff:ff:ff    
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s3  

## 2. Analyse de trames  
  
**🌞Analyse de trames**  

 [tp3-arp](./tp3_arp.pcap)

## 1. Mise en place du routage  
   
**🌞Ajouter les routes statiques nécessaires pour que john et marcel puissent se ping**  
``` shell
    [matheo@john network-scripts]$ ping 10.3.2.12  
  PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data  
 64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.14 ms
 64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.52 ms  
 64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=1.67 ms  
 64 bytes from 10.3.2.12: icmp_seq=4 ttl=63 time=1.17 ms   
 --- 10.3.2.12 ping statistics ---  
 4 packets transmitted, 4 received, 0% packet loss, time 3006ms  
 rtt min/avg/max/mdev = 1.137/1.375/1.673/0.229 ms  
```
## 2. Analyse de trames

**🌞Analyse des échanges ARP**  


## 3. Accès internet  

**🌞Donnez un accès internet à vos machines**
``` shell
    [matheo@router ~]$ ping 8.8.8.8  
 PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.  
 64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=30.7 ms  
 64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=30.3 ms  
 64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=28.2 ms  
 64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=31.2 ms  
  
 --- 8.8.8.8 ping statistics ---  
 4 packets transmitted, 4 received, 0% packet loss, time 3008ms 
 rtt min/avg/max/mdev = 28.182/30.092/31.181/1.148 ms  
```
 **🌞Donnez un accès internet à vos machines - config clients**
```shell
 [matheo@john ~]$   ping 8.8.8.8  
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.  
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=229 ms  
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=314 ms 
    64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=388 ms  
  
    --- 8.8.8.8 ping statistics ---  
    3 packets transmitted, 3 received, 0% packet loss, time 2003ms  
    rtt min/avg/max/mdev = 228.934/310.259/388.075/65.016 ms  
```
``` shell   
 [matheo@marcel ~]$ ping 8.8.8.8    
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.    
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=429 ms  
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=406 ms  
    64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=413 ms  
  
    --- 8.8.8.8 ping statistics ---  
    3 packets transmitted, 3 received, 0% packet loss, time 2000ms  
    rtt min/avg/max/mdev = 405.905/416.236/429.367/9.780 ms  
 ```     
**🌞Analyse de trames**  
  
| ordre | type trame |  IP source |     MAC source    | IP destination |  MAC destination  |   |  
|:-----:|:----------:|:----------:|:-----------------:|:--------------:|:-----------------:|:-:|  
| 1     | ping       | 10.3.1.254 | 08:00:27:c0:77:41 | 10.3.1.11      | 08:00:27:85:1a:f6 |   |  
| 2     | pong       | 10.3.1.11  | 08:00:27:7e:27:fb | 10.3.1.254     | 08:00:27:e1:f8:9a |   |  
  
 