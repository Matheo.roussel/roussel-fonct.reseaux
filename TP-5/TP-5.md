## TP5 : TCP, UDP et services réseau  

## I. First steps

**🌞 Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

 Discord TCP
 Youtube UDP
 Outlook TCP
 Twitch TCP
 Gmail TCP

**🌞 Demandez l'avis à votre OS**


 Connexions actives  :
  
  Proto  Adresse locale         Adresse distante       État  
  TCP    10.33.71.26:49577      20.199.120.85:443      ESTABLISHED  
  TCP    10.33.71.26:63793      13.32.145.117:443      ESTABLISHED  
  TCP    10.33.71.26:63807      18.244.28.23:443       ESTABLISHED  
  TCP    10.33.71.26:63812      52.111.202.3:443       TIME_WAIT  
  TCP    10.33.71.26:63823      51.11.192.48:443       ESTABLISHED  
  TCP    10.33.71.26:63825      20.74.32.47:443        ESTABLISHED  
  TCP    10.33.71.26:63827      52.111.231.0:443       ESTABLISHED  
  TCP    10.33.71.26:63835      52.98.163.18:443       ESTABLISHED  
  TCP    10.33.71.26:63838      142.251.209.46:443     ESTABLISHED  
  
  

## 2. Routage

**🌞 Prouvez que**
``` shell

 [matheo@node1 ~]$ [matheo@node1 ~]$ ping 8.8.8.8  
 PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.  
 64 bytes from 8.8.8.8: icmp_seq=1 ttl=116 time=34.5 ms  
 64 bytes from 8.8.8.8: icmp_seq=2 ttl=116 time=28.6 ms  
 64 bytes from 8.8.8.8: icmp_seq=3 ttl=116 time=30.2 ms  
 64 bytes from 8.8.8.8: icmp_seq=4 ttl=116 time=35.9 ms  

 [matheo@node1 ~]$ sudo ping 172.67.74.226  
 [sudo] password for matheo:   
 PING 172.67.74.226 (172.67.74.226) 56(84) bytes of data.  
 64 bytes from 172.67.74.226: icmp_seq=1 ttl=56 time=35.5 ms  
 64 bytes from 172.67.74.226: icmp_seq=2 ttl=56 time=38.5 ms  
 64 bytes from 172.67.74.226: icmp_seq=3 ttl=56 time=36.9 ms  
 64 bytes from 172.67.74.226: icmp_seq=4 ttl=56 time=38.1 ms  
 --- 172.67.74.226 ping statistics ---  
 4 packets transmitted, 4 received, 0% packet loss, time 3007ms    
 rtt min/avg/max/mdev = 35.457/37.244/38.530/1.191 ms  
 ```

## 3. Serveur Web  

**🌞 Installez le paquet nginx**  
``` shell
 [matheo@web ~]$ sudo dnf install nginx  
```
**🌞 Donner les bonnes permissions**
``` shell
 [matheo@web ~]$sudo chown -R nginx:nginx /var/www/site_web_nul
 ```

**🌞 Créer le site web**
``` shell
 [matheo@web ~]$ cd /var/  
 [matheo@web var]$ sudo mkdir www  
 [matheo@web var]$ cd www  
 [matheo@web www]$ sudo mkdir site_web_nul  
 [matheo@web www]$ cd site_web_nul/     
 [matheo@web site_web_nul]$ sudo touch index.html  
 [matheo@web site_web_nul]$ sudo nano index.html  
 [matheo@web site_web_nul]$ cat index.html    
 <h1>MEOW</h1>    
 ```
  
  
**🌞 Donner les bonnes permissions**  
``` shell
 [matheo@web site_web_nul]$ sudo chown -R nginx:nginx /var/www/site_web_nul    
 ```
  
**🌞 Créer un fichier de configuration NGINX pour notre site web**  
  ``` shell
 [matheo@web /]$ cd /etc/nginx/conf.d/  
 [matheo@web conf.d]$ sudo nano site_web_nul.conf  
 [matheo@web conf.d]$ cat site_web_nul.conf  
server {  
   le port sur lequel on veut écouter  
  listen 80;  
  
   le nom de la page d'accueil si le client de la précise pas  
  index index.html;  
  
   un nom pour notre serveur (pas vraiment utile ici, mais bonne pratique)  
  server_name www.site_web_nul.b1;  
  
   le dossier qui contient notre site web  
  root /var/www/site_web_nul;  
}  
```
  
**🌞 Démarrer le serveur web !**
  ``` shell
 [matheo@web conf.d]$ sudo systemctl start nginx  

 [matheo@web ~]$ systemctl status nginx  
 ● nginx.service - The nginx HTTP and reverse proxy server  
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disabled)  
     Active: active (running) since Thu 2023-11-16 17:56:37 CET; 10s ago  
    Process: 1326 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)  
    Process: 1327 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)  
    Process: 1328 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)  
   Main PID: 1329 (nginx)  
      Tasks: 2 (limit: 4606)  
     Memory: 3.4M  
        CPU: 32ms  
     CGroup: /system.slice/nginx.service  
             ├─1329 "nginx: master process /usr/sbin/nginx"  
             └─1330 "nginx: worker process"  
  ```

**🌞 Ouvrir le port firewall**
  
  ``` shell
 [matheo@web conf.d]$ sudo firewall-cmd --add-port=80/tcp --permanent  
 success  
 [matheo@web conf.d]$ sudo firewall-cmd --reload  
 success  
 [matheo@web conf.d]$  
```

**🌞 Visitez le serveur web !**
  
 http//10.5.1.12  
 MEOW  
``` shell
 [matheo@node1 ~]$ sudo curl http://10.5.1.12  
 **<h1>MEOW</h1>**


 [matheo@web ~]$ ss -nlt  
 State---------Recv-Q--------Send-Q---------------Local Address:Port---------------Peer Address:Port-------Process  
 LISTEN-----------0-----------128---------------------0.0.0.0:22------------------------0.0.0.0:*  
 LISTEN-----------0-----------511---------------------0.0.0.0:80------------------------0.0.0.0:*  
 LISTEN-----------0-----------128------------------------[::]:22-------------------------[::]:*  
 **LISTEN---------0-----------511------------------------[::]:80-------------------------[::]:**  
```
**🌞 Analyse trafic**

  
 



