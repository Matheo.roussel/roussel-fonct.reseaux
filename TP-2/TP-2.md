## I. Setup IP

**🌞 Mettez en place une configuration réseau fonctionnelle entre les deux machines**  

    - rendu
        - ip choisi : 172.31.18.102/30
                    172.31.18.101/30
        - masque : 255.255.25.252

        - reseau : 172.31.18.100

        - broadcast : 172.31.18.103

    *ça c'est la theorie, en pratique je n'y arrive pas avec ma VM*


**🌞 Prouvez que la connexion est fonctionnelle entre les deux machines** 

    PS C:\Users\mathi> ping 169.254.98.62  
  
    Envoi d’une requête 'Ping'  169.254.98.62 avec 32 octets de données :  
    Réponse de 169.254.98.62 : octets=32 temps<1ms TTL=128  
    Réponse de 169.254.98.62 : octets=32 temps<1ms TTL=128  
    Réponse de 169.254.98.62 : octets=32 temps<1ms TTL=128  
    Réponse de 169.254.98.62 : octets=32 temps<1ms TTL=128  

    Statistiques Ping pour 169.254.98.62:  
        Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),  
    Durée approximative des boucles en millisecondes :  
        Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms  

**🌞 Wireshark it**

    c'est un type 0

## II. ARP my bro

**🌞 Check the ARP table**

    PS C:\Users\mathi> arp -a  
   
    Interface : 172.20.10.2 --- 0x8  
     Adresse Internet      Adresse physique      Type  
        172.20.10.1           16-94-6c-c0-b1-64     dynamique  
        172.20.10.15          ff-ff-ff-ff-ff-ff     statique  
        224.0.0.22            01-00-5e-00-00-16     statique  
        224.0.0.251           01-00-5e-00-00-fb     statique  
        224.0.0.252           01-00-5e-00-00-fc     statique  
        239.255.255.250       01-00-5e-7f-ff-fa     statique  
        255.255.255.255       ff-ff-ff-ff-ff-ff     statique  

  MAC voisin : 00-50-56-C0-00-08  

**🌞 Manipuler la table ARP**

    