# TP-1 Séance-1  
  
## 1. Affichage d'informations sur la pile TCP/IP locale  
  
**1-🌞 Affichez les infos des cartes réseau de votre PC :**  

    Commande : ipconfig  
    PS C:\Users\mathi>  
    Carte réseau sans fil Wi-Fi :

    Suffixe DNS propre à la connexion. . . :
    Description. . . . . . . . . . . . . . : RZ608 Wi-Fi 6E 80MHz
    Adresse physique . . . . . . . . . . . : 00-41-0E-2B-91-5D
    DHCP activé. . . . . . . . . . . . . . : Oui
    Configuration automatique activée. . . : Oui
    Adresse IPv6 de liaison locale. . . . .: fe80::630e:d4bc:3b18:2eca%8(préféré)
    Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.38(préféré)
    Masque de sous-réseau. . . . . . . . . : 255.255.252.0
    Bail obtenu. . . . . . . . . . . . . . : mercredi 11 octobre 2023 13:33:20
    Bail expirant. . . . . . . . . . . . . : jeudi 12 octobre 2023 13:33:16
    Passerelle par défaut. . . . . . . . . : 10.33.51.254
    Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254
    IAID DHCPv6 . . . . . . . . . . . : 134234382
    DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2C-08-DE-FD-00-0E-C6-55-3A-BF
    Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
  
**2-🌞 Affichez votre gateway :**  

    Commande : ipconfig /all  
    PS C:\Users\mathi>  
    Gateway. . . . . . . . . : 10.33.51.254  
  
**3-🌞 Déterminer la MAC de la passerelle :**

    Commande : arp -a  
    PS C:\Users\mathi>  
    MAC de la passerelle : 7c-5a-1c-cb-fd-a4  
  
**4-🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS) :**

    1/Panneau de configuration  
    2/Réseaux et internet  
    3/Centre réseaux et partage    
    4/Modifier les paramètres de la carte  
    5/Details  
  
## 2. Modifications des informations  
  
**1-🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP :**  

    PS C:\Users\mathi>
    Carte réseau sans fil Wi-Fi :

    Suffixe DNS propre à la connexion. . . :
    Adresse IPv6 de liaison locale. . . . .: fe80::630e:d4bc:3b18:2eca%8
    Adresse IPv4. . . . . . . . . . . . . .: 10.33.51.89
    Masque de sous-réseau. . . . . . . . . : 255.255.252.0
    Passerelle par défaut. . . . . . . . . : 10.33.51.254
  
 #  3. Modification d'adresse IP  

**1-🌞 Modifiez l'IP des deux machines pour qu'elles soient dans le même réseau**

    PS C:\Users\mathi>  
    Carte Ethernet Ethernet :    
    Suffixe DNS propre à la connexion. . . :    
    Adresse IPv6 de liaison locale. . . . .: fe80::5613:62aa:2eda:9906%2    
    Adresse IPv4. . . . . . . . . . . . . .: 10.10.10.90    
    Masque de sous-réseau. . . . . . . . . : 255.255.255.0    
    Passerelle par défaut. . . . . . . . . :     
      
## II. Exploration locale en duo  

**🌞 Vérifier à l'aide d'une commande que votre IP a bien été changée**

    Carte Ethernet Ethernet :

    Suffixe DNS propre à la connexion. . . :

    Adresse IPv6 de liaison locale. . . . .: fe80::5613:62aa:2eda:9906%2

    Adresse IPv4. . . . . . . . . . . . . .: 10.10.10.90

    Masque de sous-réseau. . . . . . . . . : 255.255.255.0

    Passerelle par défaut. . . . . . . . . :

**🌞 Vérifier que les deux machines se joignent**

    Envoi d’une requête 'Ping' 10.10.10.213 avec 32 octets de données :  
  
    Réponse de 10.10.10.213 : octets=32 temps=4 ms TTL=128  
  
    Réponse de 10.10.10.213 : octets=32 temps=4 ms TTL=128  
  
    Réponse de 10.10.10.213 : octets=32 temps=4 ms TTL=128  
  
    Réponse de 10.10.10.213 : octets=32 temps=5 ms TTL=128  

**🌞 Déterminer l'adresse MAC de votre correspondant**

    10.10.10.213 40-c2-ba-10-d5-74 dynamique

    Petit chat privé

**🌞 Visualiser la connexion en cours**

    TCP 10.33.48.39:139 0.0.0.0:0 LISTENING


## III. Manipulations d'autres outils/protocoles côté client
  
## 1. DHCP  
    
**1-🌞Exploration du DHCP, depuis votre PC :**  

     Commande : ipconfig /all  
        PS C:\Users\mathi>  
        Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254  
        Bail obtenu. . . . . . . . . . . . . . : mercredi 11 octobre 2023 13:33:20
        Bail expirant. . . . . . . . . . . . . : jeudi 12 octobre 2023 13:33:16  
  
  
## 2. DNS
  
**1-🌞 Trouver l'adresse IP du serveur DNS que connaît votre ordinateur**  

         PS C:\Users\mathi>    
        Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                                8.8.8.8       
  
**2-🌞 Utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main**
  
        PS C:\Users\mathi> nslookup google.com 8.8.8.8
    Serveur :   dns.google
    Address:  8.8.8.8
    
    Réponse ne faisant pas autorité :
    Nom :    google.com
    Addresses:  2a00:1450:4007:818::200e
          142.250.179.110  
  
    
            PS C:\Users\mathi> nslookup ynov.com 8.8.8.8
    Serveur :   dns.google
    Address:  8.8.8.8
  
        Réponse ne faisant pas autorité :
    Nom :    ynov.com
    Addresses:  2606:4700:20::681a:ae9
            2606:4700:20::ac43:4ae2
            2606:4700:20::681a:be9
            104.26.11.233
            104.26.10.233
            172.67.74.226  
      
    -j'en conclu que Ynov possède plusieurs adresses pour rendre les connections plus fluide.  
    
       - PS C:\Users\mathi> nslookup ynov.com 8.8.8.8  
    Serveur :   dns.google
    Address:  8.8.8.8
  
        pour 78.34.2.17  
        PS C:\Users\mathi> nslookup 78.34.2.17 8.8.8.8  
    Serveur :   dns.google  
    Address:  8.8.8.8  
  
    Nom :    cable-78-34-2-17.nc.de  
    Address:  78.34.2.17  
  
        pour 231.34.113.12
        PS C:\Users\mathi> nslookup 231.34.113.12 8.8.8.8
    Serveur :   dns.google
    Address:  8.8.8.8
   
    dns.google ne parvient pas à trouver 231.34.113.12 : Non-existent domain
    
    la seconde adresse n'existe pas, elle n'est pas attribué contrairement a la première.

## IV. Wireshark

**1-🌞 Utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :**  
    
    Pas de prise RJ45  